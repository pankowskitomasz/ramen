import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryS3Component } from './delivery-s3.component';

describe('DeliveryS3Component', () => {
  let component: DeliveryS3Component;
  let fixture: ComponentFixture<DeliveryS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
