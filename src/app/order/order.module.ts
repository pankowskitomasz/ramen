import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order/order.component';
import { OrderbarComponent } from './orderbar/orderbar.component';
import { OrderlistComponent } from './orderlist/orderlist.component';


@NgModule({
  declarations: [
    OrderComponent,
    OrderbarComponent,
    OrderlistComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
