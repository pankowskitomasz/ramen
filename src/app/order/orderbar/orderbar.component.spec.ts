import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderbarComponent } from './orderbar.component';

describe('OrderbarComponent', () => {
  let component: OrderbarComponent;
  let fixture: ComponentFixture<OrderbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
