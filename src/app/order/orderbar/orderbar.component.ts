import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orderbar',
  templateUrl: './orderbar.component.html',
  styleUrls: ['./orderbar.component.sass']
})
export class OrderbarComponent implements OnInit {

  showFilters:boolean=true;

  showHideFilters(){
    this.showFilters=!this.showFilters;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
